resource "aws_iam_role" "iam_for_s3_to_sqs_lambda" {
  name = var.s3_to_sqs_lambda_role_name
  assume_role_policy = file("files/lambda_assume_role_policy.json")
}

resource "aws_iam_role_policy" "lambda_policy_for_s3_to_sqs" {
  name   = "lambda-policy-for-s3-to-sqs"
  role   = aws_iam_role.iam_for_s3_to_sqs_lambda.id
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "s3:*",
        "Resource": "${aws_s3_bucket.s3_job_offer_bucket.arn}/*"
      }
    ]
  })
}

resource "aws_iam_role_policy" "lambda_logging_for_s3_to_sqs" {
  name   = "lambda-logging-for-s3-to-sqs"
  role   = aws_iam_role.iam_for_s3_to_sqs_lambda.id
  policy = file("files/log_policy.json")
}

resource "aws_iam_role_policy" "iam_policy_for_sqs_sender" {
  name   = "lambda-policy-for-sqs-sender"
  role   = aws_iam_role.iam_for_s3_to_sqs_lambda.id
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "sqs:SendMessage",
        "Resource": aws_sqs_queue.job_offers_queue.arn
      }
    ]

  })
}

# resource aws_iam_role_policy iam_policy_for_s3


resource "aws_iam_role" "iam_for_sqs_to_dynamo_lambda" {
  name = var.sqs_to_dynamo_lambda_role_name
  assume_role_policy = file("files/lambda_assume_role_policy.json")
}


resource "aws_iam_role_policy" "lambda_logging_for_sqs_to_dynamo" {
  name   = "lambda-logging-for-sqs-to-dynamo"
  role   = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id
  policy = file("files/log_policy.json")
}

resource "aws_iam_role_policy" "iam_policy_for_dynamodb" {
  name = "lambda-policy-for-dynamodb"
  role = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "dynamodb:PutItem"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}


resource "aws_iam_role_policy" "iam_policy_for_sqs_receiver" {
  name = "lambda-policy-for-sqs-receiver"
  role = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "sqs:ReceiveMessage",
          "sqs:DeleteMessage",
          "sqs:GetQueueAttributes"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_role" "iam_for_job_api_lambda" {
  name = var.job_api_lambda_role_name
  assume_role_policy = file("files/lambda_assume_role_policy.json")
}


resource "aws_iam_role_policy" "iam_policy_for_dynamodb_all" {
  name = "lambda-policy-for-dynamodb-all"
  role = aws_iam_role.iam_for_job_api_lambda.id

  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Action": "dynamodb:*",
        "Resource": aws_dynamodb_table.job-table.arn
      }
    ]
  })
}


resource "aws_iam_role_policy" "lambda_logging_for_job_api" {
  name   = "lambda_logging_for_job_api"
  role   = aws_iam_role.iam_for_job_api_lambda.id
  policy = file("files/log_policy.json")
}

 resource "aws_iam_role_policy_attachment" "lambda_exec_policy_attach" {
   role = aws_iam_role.iam_for_job_api_lambda.name
   policy_arn = var.lambda_exec_policy_arn
 }

