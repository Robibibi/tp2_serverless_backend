resource "aws_s3_bucket" "s3_job_offer_bucket" {
  bucket = var.s3_user_bucket_name
  force_destroy = true
}


resource "aws_s3_object" "job_offers" {
  bucket = aws_s3_bucket.s3_job_offer_bucket.id
  key = "job_offers/"
}


resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.s3_job_offer_bucket.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_to_sqs_lambda.arn
    events = ["s3:ObjectCreated:*"]
    filter_suffix = ".csv"
  }
  depends_on = [aws_lambda_permission.allow_bucket]
}
