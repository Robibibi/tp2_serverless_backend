data "archive_file" "empty_zip_code_lambda" {
  type        = "zip"
  output_path = "empty_lambda_code.zip"
  source {
    content  = "hello_world"
    filename = "dummy.txt"
  }
}

resource "aws_lambda_function" "s3_to_sqs_lambda" {
  filename = data.archive_file.empty_zip_code_lambda.output_path
  function_name = var.s3_to_sqs_lambda_name
  handler = "index.handler"
  memory_size = 512
  timeout = 30
  runtime = "nodejs18.x"
  role = aws_iam_role.iam_for_s3_to_sqs_lambda.arn
  environment {
    variables = {
      QUEUE_URL = aws_sqs_queue.job_offers_queue.url
    }
  }
}


resource "aws_lambda_permission" "allow_bucket" {
  statement_id = "AllowExecutionFromS3Bucket"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_to_sqs_lambda.function_name
  principal = "s3.amazonaws.com"
  source_arn = aws_s3_bucket.s3_job_offer_bucket.arn
}


resource "aws_lambda_function" "sqs_to_dynamo_lambda" {
  filename = data.archive_file.empty_zip_code_lambda.output_path
  function_name = var.sqs_to_dynamo_lambda_name
  handler = "index.handler"
  memory_size = 512
  timeout = 30
  runtime = "nodejs18.x"
  role = aws_iam_role.iam_for_sqs_to_dynamo_lambda.arn
  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job-table.name
    }
  }
}


resource "aws_lambda_permission" "allow_sqs_queue" {
  statement_id = "AllowExecutionFromSQS"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sqs_to_dynamo_lambda.function_name
  principal = "sqs.amazonaws.com"
  source_arn = aws_sqs_queue.job_offers_queue.arn
}


resource "aws_lambda_function" "job_api_lambda" {
  filename = data.archive_file.empty_zip_code_lambda.output_path
  function_name = var.job_api_lambda_name
  handler = "lambda.handler"
  memory_size = 512
  timeout = 30
  runtime = "nodejs18.x"
  role = aws_iam_role.iam_for_job_api_lambda.arn
  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.job-table.name
    }
  }
}


resource "aws_lambda_permission" "allow_api_gw" {
  statement_id = "AllowExecutionFromApiGateway"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.job_api_lambda.function_name
  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_apigatewayv2_api.job_api_gw.execution_arn}/*/*/*"
}

